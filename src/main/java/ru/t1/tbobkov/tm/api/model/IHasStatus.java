package ru.t1.tbobkov.tm.api.model;

import ru.t1.tbobkov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
