package ru.t1.tbobkov.tm.exception.field;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Incorrect number...");
    }

    public NumberIncorrectException(final String value, Throwable cause) {
        super("Error! This value \"" + value + "\" is incorrect...");
    }

}
